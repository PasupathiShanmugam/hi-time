const express = require('express');
const registerRoute = express.Router();



// User model
let Register = require('../models/Register');



// Add New Users
registerRoute.route('/create').post((req, res, next) => {

    console.log("Register", req.body)
    var data = {
        "username": req.body.username || null,
        "mobileNumber": req.body.mobileNumber || null,
        "gender": req.body.gender || null,
        "dob": req.body.dob || null,
        "newpassword": req.body.newpassword || null,
        "conformpassword": req.body.conformpassword || null,
        "profilePic": null

    }
    Register.create(data, (error, data) => {
        if (error) {
            return next(error)
        } else {

            var resObject = {
                status: 200,
                data: [{ userDetails: data }],
                statusText: "user insertion success",

            };
            res.json(resObject)
        }
    })
});


// Get All Register users
registerRoute.route('/').get((req, res) => {
    console.log('get route hit');
    Register.find((error, data) => {
        if (error) {
            return next(error)
        } else {
            res.json({
                result: 200,
                status: 200,
                statusText: "All user from Provider list",
                data: [data]
            })
        }
    })
})


// Get single User
registerRoute.route('/read/:id').get((req, res) => {
    console.log('get id route hit');
    Register.findById(req.params.id, (error, data) => {
        if (error) {
            return next(error)
        } else {
            // pvdataa
            // Provider
            res.json({
                status: 200,
                statusText: "User details fetch succsfully",
                data: [data],

            })
        }
    })
})


//Address GEt Login User Exist  
registerRoute.route('/login').post((req, res) => {
    console.log('get id route hit', req.body);
    Register.find(req.body, (error, data, next) => {
        if (error) {
            res.json({
                status: 400,
                statusText: error,

            })
        } else {

            if (data.length != 0) {
                res.json({
                    status: 200,
                    statusText: "user already exist",

                })
            } else {
                res.json({
                    status: 400,
                    statusText: "No user exist",

                })
            }

        }
    })
})



// Forgot password
registerRoute.route('/update-password/:id').post((req, res) => {

    console.log('Update-Password', req.params.id);

    Register.findByIdAndUpdate(req.params.id, {

        $set: { newpassword: req.body.newpassword, conformpassword: req.body.conformpassword }

    }, (error, next) => {
        if (error) {
            return next(error);
        } else {
            res.json({

                status: 200,
                statusText: "Password Update succsfully",

            })

        }
    })
});




module.exports = registerRoute;