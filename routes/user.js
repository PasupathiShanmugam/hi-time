const express = require('express');
const userRoute = express.Router();

// User model
let User = require('../models/User');

// Add New Users
userRoute.route('/create').post((req, res, next) => {

    console.log("Register", req.body)
    var data = {
        "templates": req.body.templates,
    }
    User.create(data, (error, data) => {
        if (error) {
            return next(error)
        } else {

            var resObject = {
                status: 200,
                statusText: "template insert successfully",
                _id: data._id,
                templates: data.templates

            };
            res.json(resObject)
        }
    })
});


// Get All Template users
userRoute.route('/').get((req, res) => {
        console.log('get route hit');
        User.find((error, data) => {
            if (error) {
                return next(error)
            } else {
                res.json({
                    result: 200,
                    status: 200,
                    statusText: "All user from Template list",
                    data: [data]
                })
            }
        })
    })
    // Get single User Templates
userRoute.route('/read/:id').get((req, res) => {
    console.log('get id route hit');
    User.findById(req.params.id, (error, data) => {
        if (error) {
            return next(error)
        } else {
            // pvdataa
            // Provider
            res.json({
                status: 200,
                statusText: "User details fetch succsfully",
                data: [data],

            })
        }
    })
})



// Template Update 
userRoute.route('/update-templates/:id').post((req, res) => {

    console.log('Update-templates', req.params.id);

    User.findByIdAndUpdate(req.params.id, {

        $set: { templates: req.body.templates, }

    }, (error, next) => {
        if (error) {
            return next(error);
        } else {
            res.json({

                status: 200,
                statusText: "Template Update succsfully",

            })

        }
    })
});

// Template Update 
userRoute.route('/delete-templates/:id').post((req, res) => {

    console.log('Update-templates', req.params.id);

    User.findOneAndRemove(req.params.id, {


    }, (error, next) => {
        if (error) {
            return next(error);
        } else {
            res.json({

                status: 200,
                statusText: "Template Delete succsfully",

            })

        }
    })
});

module.exports = userRoute;