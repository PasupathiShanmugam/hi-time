const express = require('express');
const adminRoute = express.Router();
const jwt = require('jsonwebtoken');
const auth = require('../controller/authorizer');
var admin = require('../models/Admin');
var nodemailer = require('nodemailer');


adminRoute.route('/subadmin/register').post((req, res, next) => {
    console.log('create post route hit');
    admin.find({ email: req.body.email }).then(data => {
        if (data.length != 0) {
            res.json({ status: "failure", message: "Email is already registered" });
        } else {
            admin.create(req.body, (error, data) => {
                if (error) {
                    res.json({ status: "failure", message: error.message });
                } else {
                    console.log(data)
                    res.json({
                        status: "success", message: "Registered successfully",
                        data: data
                    })
                }
            })
        }
    }).catch(err => {
        res.json({ status: "failure", message: "Internal server error" });
    });

});




adminRoute.route('/email').post((req, res, next) => {
    var mailId = req.body.email;
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'bigphelpsdxminds@gmail.com',
            pass: 'bigphelps1234'
        },      
        tls: {
            rejectUnauthorized: false
        }
    });
    var mailOptions = {
        from: 'bigphelpsdxminds@gmail.com',
        to: mailId,
        subject: 'Sending Email using Node.js',
        html: '<h1>Here is your OTP:</h1><p>' + "hi" + '</p>'
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error.message);
            res.json({ status: false, message: "falied to send mail to the user" })
        } else {
            res.json({ status: true, message: "sent mail to the user" })
            console.log('Email sent: ' + info.response);
        }
    });

});


adminRoute.route('/login').post((req, res, next) => {
    console.log('create post route hit', req.body);
    admin.find({ email: req.body.email }).then(data => {
        console.log("hih", data, data[0].password)
        if (data.length != 0) {
            if (data[0].password != req.body.password) {
                console.log(data[0].password, req.body.password)
                res.json({ status: "failure", "message": "Entered invalid password" });

            } else {
                var id = data[0]._id
                var token = jwt.sign({ id }, 'Secretkey', (err, token) => {
                    if (err) {
                        res.json({ status: "failure", "message": "Entered invalid password" });
                    } else {
                        res.json({
                            "status": "success", "message": "Login successfully",
                            "data": data,
                            "token": token
                        })
                    }
                })

            }
        } else {
            res.json({ status: "failure", message: "Entered invalid email" });
        }
    }).catch(err => {
        res.json({ status: "failure", message: err.message });
    })

});

//get all list of admin and subadmin
adminRoute.route('/subadmin').get((req, res, next) => {
    console.log('get data subadmin', req.body);
    admin.find({}).then(data => {
        if (data.length != 0) {
            var list = data.filter(data => data.adminstration == "subadmin")
            res.json({
                "status": "success", "message": "Successfully subadmin list fetched",
                "data": list
            })
        } else {
            res.json({ status: "failure", message: "No data found" });
        }
    }).catch(err => {
        res.json({ status: "failure", message: err.message });
    })

});


//get single subadmin detail
adminRoute.route('/subadmin/:id').get((req, res, next) => {
    console.log('get data subadmin', req.body);
    admin.find({ _id: req.params.id }).then(data => {
        if (data.length != 0) {
            var list = data.filter(data => data.role == "subadmin")
            res.json({
                "status": "success", "message": "Successfully subadmin list fetched",
                "data": list
            })
        } else {
            res.json({ status: "failure", message: "No data found" });
        }
    }).catch(err => {
        res.json({ status: "failure", message: err.message });
    })

});


module.exports = adminRoute;