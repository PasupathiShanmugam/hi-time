const express = require('express');
const app = express();
const cors = require('cors');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dbConfig = require('./database/database');

const registerRoute = require('./routes/register');
const userRoute = require('./routes/user');
const adminRoute = require('./routes/admin');

const PORT = 1000;

// Allow all origin requests
app.use(cors())

// Accept data formats as below
app.use(bodyParser.raw());
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());


app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        next();
    })
    // Find 404 and hand over to error handle+







// Connecting with mongo db
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.db, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
        console.log('Database sucessfully connected')
    },
    error => {
        console.log('Database could not connected: ' + error)
    }
)

app.use(express.static(path.join(__dirname, 'dist/mean-stack-crud-app')));
app.use('/', express.static(path.join(__dirname, 'dist/mean-stack-crud-app')));

app.use('/user', userRoute);
app.use('/admin', adminRoute)
app.use('/register', registerRoute)


app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    console.error(err.message); // Log error message in our server's console
    if (!err.statusCode) err.statusCode = 500; // If err has no specified error code, set error code to 'Internal Server Error (500)'
    res.status(err.statusCode).send(err.message); // All HTTP requests must have a response, so let's send back an error with its status code and message
});




app.listen(PORT, () => console.log('Listening to PORT :', PORT));