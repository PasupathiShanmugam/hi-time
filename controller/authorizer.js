const jwt = require('jsonwebtoken');
const router = require('express').Router();

const secretKey = 'BigPHelpSecretKey'

module.exports = {
    generateToken : payload => {
        jwt.sign(payload , secretKey, (err,token) => {
            return token;
        });
    },

    verifyToken : (req, res, next) => {
        const bearerHeader = req.headers['authorization'];
        if(bearerHeader && typeof(bearerHeader) !== undefined) {
            const bearerToken = bearerHeader.split(' ')[1];
            jwt.verify( bearerToken, secretKey, (err,authData) => {
                if(err) {
                    console.log('token invalid')
                    return next(err);
                } else {
                    console.log('token valid');
                    return { message : 'User Authenticated' }
                }
            });
            next();
        } else {
            // Forbidden
            res.sendStatus(403);
        }
    }
}