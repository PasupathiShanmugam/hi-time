module.exports = {
    broadcastRequest : data => {
        console.log('broadCastRequest says ',data)
        return Math.floor(Math.random() * 100);
    },
    generateRequest : request => {
        var generateRequestPromise = new Promise( async (resolve, reject) => {
            var timeCount = 0; var timeInterval = 5000;
            var timeLimit = request.responseWaitTime * 60 * 1000;
            console.log('TimeLimit',timeLimit);
            var timerInterval = setInterval(request => {
                if(timeCount <= timeLimit) {
                    var broadCaster = module.exports.broadcastRequest('first');
                    console.log('Return value from broadcaster',broadCaster);
                    if(((broadCaster % 2) == 0)){
                        console.log('return value is even, resolve it');
                        resolve(broadCaster);
                        clearInterval(timerInterval);
                    } else {
                        broadCaster = module.exports.broadcastRequest('odd');
                    }
                    console.log('Time Count',timeCount);
                    timeCount+= timeInterval
                } else {
                    clearInterval(timerInterval);
                    resolve({ message : 'Request time ended!'})
                }
            }, timeInterval )
        });
        return generateRequestPromise;
    }
}