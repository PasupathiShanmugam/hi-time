// Import all Mongo Models
const User = require('../models/User');

module.exports = {
    chooseModel: model => {
        switch (model) {

            case 'User':
                return User

            default:
                return { status: 'error', message: 'Invalid model name' }
        }
    },
    createOne: (model, data) => {
        var createPromise = new Promise((resolve, reject) => {
            model.create(data, (error, data) => {
                if (error) reject(error)
                else resolve(data)
            });
        });
        return createPromise;
    },
    findAll: (model) => {
        var findAllPromise = new Promise((resolve, reject) => {
            model.find((error, data) => {
                if (error) reject(error)
                else resolve(data)
            })
        });
        return findAllPromise;
    },
    findWithFilter: (model, filter) => {
        console.log('Filter at findWithFilter', filter);
        var findWithFilterPromise = new Promise((resolve, reject) => {
            model.find(filter, (error, data) => {
                if (error) reject(error)
                else resolve(data)
            })
        });
        return findWithFilterPromise;
    },
    findById: (model, id, filter) => {
        if (filter) {
            var findAllPromise = new Promise((resolve, reject) => {
                model.findById(id, filter, (error, data) => {
                    if (error) reject(error)
                    else resolve(data)
                })
            });
            return findAllPromise;
        } else {
            var findAllPromise = new Promise((resolve, reject) => {
                model.findById(id, (error, data) => {
                    if (error) reject(error)
                    else resolve(data)
                })
            });
            return findAllPromise;
        }
    },
    findByIdAndUpdate: (model, id, modifyObject, options) => {
        if (options) {
            console.log('Options present');
            var findAndUpdatePromise = new Promise((resolve, reject) => {
                model.findByIdAndUpdate(id, {
                    $set: modifyObject
                }, options, (error, data) => {
                    if (error) reject(error);
                    else resolve(data)
                })
            });
            return findAndUpdatePromise;
        } else {
            console.log('Options absent');
            var findAndUpdatePromise = new Promise((resolve, reject) => {
                model.findByIdAndUpdate(id, {
                    $set: modifyObject
                }, (error, data) => {
                    if (error) reject(error);
                    else resolve(data)
                })
            });
            return findAndUpdatePromise;
        }
    },
    addElementToArray: (model, id, modifyObject) => {
        var addElementToArray = new Promise((resolve, reject) => {
            model.findByIdAndUpdate(id, {
                $push: modifyObject
            }, (error, data) => {
                if (error) reject(error)
                else resolve(data)
            })
        });
        return addElementToArray;
    },
    removeElementFromArray: (model, id, modifyObject) => {
        var addElementToArray = new Promise((resolve, reject) => {
            model.findByIdAndUpdate(id, {
                $pull: modifyObject
            }, (error, data) => {
                if (error) reject(error)
                else resolve(data)
            })
        });
        return addElementToArray;
    },
    update: (model, conditions, modifyObject, options) => {
        if (options) {
            var updatePromise = new Promise((resolve, reject) => {
                model.update(conditions, modifyObject, options, (error, data) => {
                    if (error) reject(error)
                    else resolve(data)
                });
            });
            return updatePromise
        } else {
            var updatePromise = new Promise((resolve, reject) => {
                model.update(conditions, modifyObject, (error, data) => {
                    if (error) reject(error)
                    else resolve(data)
                });
            });
            return updatePromise
        }
    },
    sortDescend: (model, limit, next) => {
        var sortDescendPromise = new Promise((resolve, reject) => {
            if (limit) {
                model.aggregate([
                    { $match: {} },
                    {
                        $group: {
                            _id: "$service",
                            count: { $sum: 1 },
                        },
                    },
                ], (error, data) => {
                    if (error) {
                        reject(error);
                        return next(error);
                    } else {
                        var resObj = {
                            data: data,
                            statusCode: 200,
                            statusText: "success"
                        }
                        resolve(resObj)
                    }
                }).sort({ count: -1 }).limit(6);
            } else {
                model.aggregate([
                    { $match: {} },
                    {
                        $group: {
                            _id: "$service",
                            count: { $sum: 1 },
                        },
                    },
                ], (error, data) => {
                    if (error) {
                        reject(error);
                        return next(error);
                    } else {
                        var resObj = {
                            data: data,
                            statusCode: 200,
                            statusText: "success"
                        }
                        resolve(resObj)
                    }
                }).sort({ count: -1 })
            }
        })
        return sortDescendPromise;
    },
    recentServices: (model, userID, limit, next) => {
        var recentServicesPromise = new Promise((resolve, reject) => {
            console.log('userId at recentservices', userID);
            if (limit) {
                model.find({ userId: userID }, (error, data) => {
                    if (error) {
                        reject(error);
                        return next(error)
                    } else {
                        resolve(data);
                    }
                }).sort({ createdAt: -1 }).limit(6)
            } else {
                model.find({ userId: userID }, (error, data) => {
                    if (error) {
                        reject(error);
                        return next(error)
                    } else {
                        resolve(data);
                    }
                }).sort({ createdAt: -1 })
            }
        })
        return recentServicesPromise;
    }
}