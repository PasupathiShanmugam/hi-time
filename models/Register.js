const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Define collection and schema
let rejesterSchema = new Schema({
    username: {
        type: String,

    },
    mobileNumber: {
        type: String,

        trim: true,
        unique: true
    },

    gender: {
        type: String,
        trim: true
    },
    dob: {
        type: String,
        trim: true
    },
    newpassword: {
        type: String,

        trim: true
    },
    conformpassword: {
        type: String,

        trim: true
    },
    profilePic: {
        type: String,
    }

}, {
    register: 'register'
})

module.exports = mongoose.model('register', rejesterSchema)