const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let access = new Schema({

    name: {
        type: String
    },
    read: {
        type: Boolean
    },
    write: {
        type: Boolean
    },
    edit: {
        type: Boolean
    },
    delete: {
        type: Boolean
    }


});


// Define collection and schema
let adminSchema = new Schema({
    username: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    region: {
        type: String,
        trim: true
    },
    adminstration: {
        type: String,
        trim: true
    },
    role: {
        type: String,
        trim: true
    },
    status: {
        type: Number,
        required: true
    },
    accessControl: [access]
}, {
    collection: 'admin'
})

module.exports = mongoose.model('admin', adminSchema)